package com.simbirsoft.demotesttask.controller;

import com.simbirsoft.demotesttask.entity.User;
import com.simbirsoft.demotesttask.service.api.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@RequiredArgsConstructor
public class MainController {

    private final UserService userService;

    @GetMapping("/")
    public String getIndex(Model model){
        model.addAttribute("user", new User());
        return "index";
    }

    @PostMapping("/save")
    public String addUser(User user){
        userService.addUserInfo(user);
        return "redirect:/";
    }
}
