package com.simbirsoft.demotesttask.repository;

import com.simbirsoft.demotesttask.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
