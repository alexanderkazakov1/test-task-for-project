package com.simbirsoft.demotesttask.service.impl;

import com.simbirsoft.demotesttask.entity.User;
import com.simbirsoft.demotesttask.repository.UserRepository;
import com.simbirsoft.demotesttask.service.api.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User addUserInfo(User user) {
        return userRepository.save(user);
    }
}
