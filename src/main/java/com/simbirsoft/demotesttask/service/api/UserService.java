package com.simbirsoft.demotesttask.service.api;

import com.simbirsoft.demotesttask.entity.User;

public interface UserService {
    User addUserInfo(User user);
}
